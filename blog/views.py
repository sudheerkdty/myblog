from django.shortcuts import render, get_object_or_404
from .models import Blog,Comment
from .forms import CommentForm
from django.contrib import messages

from django.views.generic import ListView

class PostListView(ListView):
    queryset = Blog.published.all()
    context_object_name = 'posts'
    template_name = 'post_list.html'


def post_detail(request, pk):
    # post = get_object_or_404(Blog, slug=post,
    #                                status='published',
    #                                publish__year=year,
    #                                publish__month=month,
    #                                publish__day=day)
    post = get_object_or_404(Blog, pk=pk)
    comments = post.comments.filter(active=True)

    new_comment = None
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():

            new_comment = comment_form.save(commit=False)
            new_comment.post = post
            new_comment.active = True
            new_comment.save()
            messages.success(request, 'Your comment was updated successfully!')
            # messages.success(request, 'comment added')
            # messages.add_message(self.request, messages.INFO, 'Hello world.')

    else:
        comment_form = CommentForm()
    return render(request,
                  'post_detail.html',
                  {'post': post,
                   'comments': comments,
                   'new_comment': new_comment,
                   'comment_form': comment_form})



